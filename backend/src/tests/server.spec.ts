//Begin: 20221221 JEST
import { FastifyInstance } from 'fastify'
import { startFastify } from '../server'
describe('Server test', () => {
    let server: FastifyInstance
    beforeAll(async () => {
        server = startFastify(8888)
        await server.ready()
    })
    
    afterAll(async () => {
        try {
            await server.close()
            console.log('Closing Fastify server is done!')
        } catch (e) {
            console.log(`Failed to close a Fastify server, reason: ${e}`)
        }
    })
    it('should successfully get a pong string', async () => {
        const response = await server.inject({ method: 'GET', url: '/ping' })
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(JSON.stringify({ msg: 'pong' }))
    })
})
//End: 20221221 JEST