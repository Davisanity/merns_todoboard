import { FastifyInstance } from 'fastify'
import { startFastify } from '../server'
import * as dbHandler from './db'
import { Imemo } from '../types/memo'
describe('Memo API test', () => {
  let server: FastifyInstance
  const fastifyPort = 8888
  beforeAll(async () => {
    await dbHandler.connect()
    server = startFastify(fastifyPort)
    await server.ready()
  })
  afterEach(async () => {
    // await dbHandler.clearDatabase()
  })
  afterAll(async () => {
    // await dbHandler.closeDatabase()
    await server.close()
    console.log('Closing Fastify server is done!')
  })
  it('should successfully get a empty list of memos', async () => {
    const response = await server.inject({ method: 'GET', url: '/api/memos' })
    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual(JSON.stringify({ memos: [] }))
  })
  it('should successfully post a memo to mongodb', async () => {
    const response = await server.inject({
      method: 'POST',
      url: '/api/memos',
      payload: {
        study: 'MERNS',
        weight: 'ABook'
      }
    })
    expect(response.statusCode).toBe(201)
    const memo: Imemo = JSON.parse(response.body)['memo']
    expect(memo.study).toBe('MERNS')
    expect(memo.read).toBe('ABook')
  })
})