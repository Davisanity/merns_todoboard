//Begin: 20221016 for mongoDB
import mongoose from 'mongoose'
const establishConnection = (connectionString: string) => {
  mongoose.connect(connectionString, (error) => {
    if (error) {
      console.log(`Error in DB connection: ${error}`)
    } else {
      console.log(`MongoDB connection successful`)
    }
  })
}
export { establishConnection }
