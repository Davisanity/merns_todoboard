//Begin: 20221016 for mongoDB
import { Imemo } from './../types/memo'
import memo from './../models/memo'
interface MemoRepo {
  getMemos(): Promise<Array<Imemo>>
  addMemos(memoBody: Imemo): Promise<Imemo>
  updateMemos(id: String, catBody: Imemo): Promise<Imemo | null>
  deleteMemos(id:string):Promise<Imemo | null>
}
class MemoRepoImpl implements MemoRepo {
  private constructor() {}
  static of(): MemoRepoImpl {
    return new MemoRepoImpl()
  }
  async getMemos(): Promise<Array<Imemo>> {
    return memo.find()
  }

  async addMemos(memoBody: Imemo): Promise<Imemo> {
    return memo.create(memoBody)
  }
  //End: 20221016
  //Begin: 20221221 
  async updateMemos(id: String, catBody: Imemo): Promise<Imemo | null> {
    return memo.findByIdAndUpdate(id, catBody, { new: true })
  }
  async deleteMemos(id:string):Promise<Imemo | null>{
    return memo.findByIdAndDelete(id)
  }
  //End: 20221221
}
export { MemoRepoImpl }
