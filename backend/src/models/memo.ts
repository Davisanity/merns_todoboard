//Begin: 20221016 for mongoDB
import { model, Schema } from 'mongoose'
import { Imemo } from '../types/memo'
const memoSchema: Schema = new Schema(
  {
    date: {
      type: Date,
      required: true
    },
    study: {
      type: String,
      default: null
    },
    language: {
      type: String,
      default: null
    },
    watch: {
      type: String,
      default: null
    },
    exercise: {
      type: String,
      default: null
    },
    read: {
      type: String,
      default: null
    },
    other: {
      type: String,
      default: null
    }
  },
  {
    timestamps: true
  }
)
memoSchema.set('toJSON', {
  virtuals: true,
  versionKey: false
})

export default model<Imemo>('memo', memoSchema)
