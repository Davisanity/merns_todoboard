import fastify, { FastifyInstance } from 'fastify'
import { establishConnection } from './plugins/mongoose' //Begin: 20221016 For mongo db practice //End
import { MemoRepoImpl } from './repo/memo-repo' //Begin: 20221016 For mongo db practice //End
import { Imemo } from './types/memo' //Begin: 20221016 For mongo db practice //End
import { MemoRouter } from './route/memo' //Begin: 20221022 For mongo db practice //End
import fastifyStatic from '@fastify/static' //Begin: 20230114 For register in static
import path from 'path'                     //End:20230114
const server: FastifyInstance = fastify({
  logger: {
    transport: {
      target: 'pino-pretty'
    },
    level: 'debug'
  }
})
const startFastify: (port: number) => FastifyInstance = (port) => {
  const listenAddress = '0.0.0.0'
  const fastifyConfig = {
    port: port,
    host: listenAddress
  }
  server.listen(fastifyConfig, (error, _) => {
    if (error) {
      console.error(error)
    }
  })

  //Begin: 20221016 For mongo db practice
  server.listen(fastifyConfig, (error, _) => {
    if (error) {
      console.error(error)
    }
    const connectionString = process.env.MONGO_CONNECTION_STRING || 'mongodb://localhost:27017/myMERN'
    if (process.env.NODE_ENV !== 'test') {
      establishConnection(connectionString)
    }
  })
  //End: 20221016 For mongo db practice

  server.get('/ping', async (request, reply) => {
    return reply.status(200).send({ msg: 'pong' })
  })
  //Begin: 20221016 For mongo db practice
  // server.get('/memos', async (request, reply) => {
  //   const memoRepo = MemoRepoImpl.of()
  //   try {
  //     const memos = await memoRepo.getMemos()
  //     return reply.status(200).send({ memos })
  //   } catch (error) {
  //     return reply.status(500).send({ msg: `Internal Server Error: ${error}` })
  //   }
  // })
  // server.post('/memos', async (request, reply) => {
  //   const memoRepo = MemoRepoImpl.of()
  //   try {
  //     const memoBody = request.body as Imemo
  //     const memo = await memoRepo.addMemos(memoBody)
  //     return reply.status(201).send({ memo })
  //   } catch (error) {
  //     return reply.status(500).send({ msg: `Internal Server Error: ${error}` })
  //   }
  // })
  //End: 20221016 For mongo db practice
  server.register(MemoRouter, { prefix: '/v1' })
  //Begin: 20230114 For register static
  server.register(fastifyStatic, {
    root: path.join(__dirname, '../../frontend/build'),
    prefix: '/'
  })
  //End: 20230114 For register static
  return server
}
export { startFastify }
