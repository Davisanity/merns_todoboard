interface Imemo {
  date: Date
  study: string
  language: string
  watch: string
  exercise: string
  read: string
  other: string
}
export { Imemo }
