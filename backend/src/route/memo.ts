import { FastifyInstance, RouteShorthandOptions, FastifyReply } from 'fastify'
import { Types } from 'mongoose'
import { MemoRepoImpl } from '../repo/memo-repo' //Begin: 20221016 For mongo db practice //End
import { Imemo } from '../types/memo' //Begin: 20221016 For mongo db practice //End
import { Type, Static } from '@sinclair/typebox'
//Begin: 20221022 For Define API routes as a plugin
  const MemoRouter = (server: FastifyInstance, opts: RouteShorthandOptions, done: (error?: Error) => void) => {
    //Begin: 20221221 
    const memosResponse = Type.Object({
      memos: Type.Array(
        Type.Object({
          id: Type.String(),
          date: Type.Date(),
          study: Type.String(),
          language: Type.String(),
          watch: Type.String(),
          exercise: Type.String(),
          read: Type.String(),
          other: Type.String()
        })
      )
    })
    type memosResponse = Static<typeof memosResponse>
    const memoResponse = Type.Object({
      memo: Type.Object(
        Type.Object({
          id: Type.String(),
          date: Type.Date(),
          study: Type.String(),
          language: Type.String(),
          watch: Type.String(),
          exercise: Type.String(),
          read: Type.String(),
          other: Type.String()
        })
      )
    })
    type memoResponse = Static<typeof memoResponse>
    opts = { ...opts, schema: { response: { 200: memosResponse, 201: memoResponse } } }

    //End: 20221221  
    server.get('/memos', async (request, reply) => {
        const memoRepo = MemoRepoImpl.of()
        try {
          const memos = await memoRepo.getMemos()
          return reply.status(200).send({ memos })
        } catch (error) {
          return reply.status(500).send({ msg: `Internal Server Error: ${error}` })
        }
    })
    server.post<{ Body: Imemo }>('/memos', async (request, reply) => {
        const memoRepo = MemoRepoImpl.of()
        try {
            const memoBody = request.body as Imemo
            const memo = await memoRepo.addMemos(memoBody)
            return reply.status(201).send({ memo })
          } catch (error) {
            return reply.status(500).send({ msg: `Internal Server Error: ${error}` })
          }
    })
    //Begin: 20221221 URL parameters
    interface IdParams {
      id: string
    }
    server.put<{ Params: IdParams; Body: Imemo}>('/memos/:id', async(request,reply) => {
      const memoRepo = MemoRepoImpl.of()
      try {
        const memoBody = request.body
        const id = request.params.id
        if(!Types.ObjectId.isValid(id)){
          return reply.status(400).send({msg: `Invalid id`})
        }
        const memo = await memoRepo.updateMemos(id, memoBody)
        if (memo) {
          return reply.status(200).send({ memo })
        } else {
          return reply.status(404).send({msg: `Memo #${id} Not Found`})
        }
      } catch (error) {
        return reply.status(500).send({ msg: error })
      }
    })

    server.delete<{ Params: IdParams }>('/memos/:id', async (request, reply) => {
      const memoRepo = MemoRepoImpl.of()
      try {
        const id = request.params.id
        if (!Types.ObjectId.isValid(id)) {
          return reply.status(400).send({ msg: `Invalid id` })
        }
        const memo = await memoRepo.deleteMemos(id)
        if (memo) {
          return reply.status(204).send()
        } else {
          return reply.status(404).send({ msg: `memo #${id} Not Found` })
        }
      } catch (error) {
        return reply.status(500).send({ msg: error })
      }
    })
    //End: 20221221 URL parameters
    done()
}
export { MemoRouter }
//End: 20221022 For Define API routes as a plugin